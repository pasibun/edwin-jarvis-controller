from enum import Enum


class HexapodMovementTypes(Enum):
    DEFAULT = 0
    TRIPOD_GAIT = 1
    CRAB_WALK = 2
